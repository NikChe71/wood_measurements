# Wood measurements

1. About project
2. How to update design and use it in app
3. How to run application

## About project
This project is simple area calculator that have two modes:
* Create rectangle details and sum their areas.
* Set available area and put in there details to understand how many of them will fit.

## To create window design
Run:
```bash
pyuic5 ./wood_window.ui -o window_design.py
```

## How to run application
Run:
```bash
python -m source.main
```

## How to build application to run on Windows machine
Install fbs:
```bash
pip install fbs pyinstaller-hooks
```
Structure of project that will be successfully build should look like this:
```bash
src
├───build
│   └───settings
└───main
    ├───icons
    │   ├───base
    │   ├───linux
    │   └───mac
    └───python
        ├───application
        ├───fix
        ├───gui
        ├───schemas
        └───main.py
```

Move source of application to python folder, remove `__init__.py` from python folder.

Run application:
```bash
fbs run
```
If it runs, then build it:
```bash
fbs freeze --debug
```
Run application
```bash
<dir>/wood.exe
```