from source.logger import log


class Detail:
    def __init__(self, width: int, height: int, origin: tuple = (0, 0)):
        self.width = width
        self.height = height
        self.origin = origin

    def area(self):
        return self.width * self.height

    @property
    def area_for_sorting(self):
        return self.width * self.height

    def rotate_by_90(self):
        self.width, self.height = self.height, self.width

    @property
    def center_point_width(self):
        return int(self.origin[0] + self.width / 2)

    @property
    def center_point_height(self):
        return int(self.origin[1] + self.height / 2)

    @property
    def coordinates(self):
        left_upper = self.origin
        right_bottom = (self.origin[0] + self.width, self.origin[1] + self.height)
        return left_upper, right_bottom

    def fit_in_main_area(self, main_area):
        main_left_upper, main_right_bottom = main_area.coordinates
        main_x0, main_y0 = main_left_upper
        main_x1, main_y1 = main_right_bottom

        detail_left_upper, detail_right_bottom = self.coordinates
        detail_x0, detail_y0 = detail_left_upper
        detail_x1, detail_y1 = detail_right_bottom

        # Check that all corners is in main area:
        if main_area.height < self.height:
            return False

        if main_area.width < self.width:
            return False

        # left upper corner
        if detail_x0 < main_x0:
            return False

        # right bottom corner
        if detail_y1 > main_y1:
            return False

        return True


class Calculator:
    def __init__(self):
        self.used_area = 0

    def calculate_area(self, details: list):
        for detail in details:
            self.used_area += detail.area()
        return self.used_area

    # TODO: Write unittests on this function
    def find_place(self, free_space: Detail, detail: Detail):
        """
        Algotithm gets correct orientation. Rotate detail if needed.
        Firstly we look at large details
        Cut area on slices by the short side.
        Like this:
        +----------+
        |////#     |
        |////#     |
        |////#     |
        |#####     |
        |    !     |
        |    !     |
        +----------+
        :param free_space:
        :param detail:
        :return: fitted detail, cutted space #1, cutted space #2
        """
        # Basic check
        if free_space.area() < detail.area():
            return

        # Rotate detail if it not fits in main free area
        if free_space.width < detail.width or free_space.height < detail.height:
            detail.rotate_by_90()
            log.info(f"Detail was oriented horizontally.")

        if free_space.height < detail.height:
            return False

        if free_space.width < detail.width:
            return False

        # Set origin of new detail
        detail.origin = free_space.origin

        # Detail fits in free area.
        # Now split area on two cutted rectangles by small side of detail
        width_is_smallest_side = False
        height_is_smallest_side = False
        if detail.width < detail.height:
            width_is_smallest_side = True
            log.info(f"Detail has smallest width")
        # Squares will be cutted by height too...)
        else:
            height_is_smallest_side = True
            log.info(f"Detail has smallest height")

        first_cut_area = None
        second_cut_area = None

        # TODO: check cases when there will be not free space for second or first cutted rectangles
        # First (smallest cut free space)
        if width_is_smallest_side:
            if free_space.height >= detail.height:
                first_cut_area = Detail(width=detail.width,
                                        height=free_space.height - detail.height,
                                        origin=(detail.origin[0] + 0, detail.origin[1] + detail.height))
                log.info(f"Cut area #1: {first_cut_area.coordinates}")
            else:
                log.error(f"Cannot cut on part. free_space.height is more than detail.height. "
                          f"{free_space.coordinates}>{detail.coordinates}")
                return

            # Second (larger cut free space)
            second_cut_area = Detail(width=free_space.width - detail.width,
                                     height=free_space.height,
                                     origin=(detail.origin[0] + detail.width, detail.origin[1] + 0))
            log.info(f"Cut area #2: {second_cut_area.coordinates}")

        if height_is_smallest_side:
            if free_space.width >= detail.width:
                first_cut_area = Detail(width=free_space.width - detail.width,
                                        height=detail.height,
                                        origin=(detail.origin[0] + detail.width, detail.origin[1] + 0))
                log.info(f"Cut area #1: {first_cut_area.coordinates}, origin: {first_cut_area.origin}")
            else:
                log.error(f"Cannot cut on part. free_space.width is more than detail.width. "
                          f"{free_space.coordinates}>{detail.coordinates}")
                return

            # Second (larger cut free space)
            second_cut_area = Detail(width=free_space.width,
                                     height=free_space.height - detail.height,
                                     origin=(detail.origin[0] + 0, detail.origin[1] + detail.height))
            log.info(f"Cut area #2: {second_cut_area.coordinates}, origin: {second_cut_area.origin}")

        return detail, first_cut_area, second_cut_area

    @staticmethod
    def update_free_spaces(free_spaces):
        # Delete all spaces which has no area to place details
        for space in free_spaces:
            space_must_be_deleted = False

            if space.area() == 0:
                space_must_be_deleted = True

            # X coordinates are the same
            if space.coordinates[0][0] == space.coordinates[1][0]:
                space_must_be_deleted = True
            # Y coordinates are the same
            if space.coordinates[0][1] == space.coordinates[1][1]:
                space_must_be_deleted = True

            if space_must_be_deleted:
                free_spaces.remove(space)

        return free_spaces
