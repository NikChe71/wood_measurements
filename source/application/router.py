from source.application.exception_handler import exc_safe


class Logic:
    def __init__(self, window_cls):
        self.window_cls = window_cls
        super().__init__()

    @exc_safe
    def add_detail(self):

        self.window_cls.textEdit.append('hello!')
