from PyQt5 import QtGui, QtCore

from source.constants import BROWN_COLOR, DARK_BROWN_COLOR, YELLOW_COLOR, DARK_YELLOW_COLOR

# Detail pen
detail_pen = QtGui.QPen(QtCore.Qt.gray)
detail_pen.setStyle(QtCore.Qt.DashLine)
detail_pen.setColor(QtGui.QColor(*DARK_YELLOW_COLOR, 127))
detail_color = QtGui.QColor(*YELLOW_COLOR, 127)

# Free space pen
free_space_pen = QtGui.QPen()
free_space_pen.setColor(QtGui.QColor(*DARK_BROWN_COLOR, 127))
free_space_color = QtGui.QColor(*BROWN_COLOR, 127)
free_space_pen.setStyle(QtCore.Qt.SolidLine)
free_space_pen.setWidth(3)