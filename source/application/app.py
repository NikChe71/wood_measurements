import json
import operator

from source.application.drawing_instruments import detail_pen, free_space_pen, free_space_color, detail_color
from source.gui import window_design
from source.logger import log
from source.application.exception_handler import exc_safe
from source.application.utils import Detail, Calculator
from source.schemas.dimensions import DetailSchema, MainAreaSchema

from PyQt5 import QtWidgets, QtGui, QtCore


class App(QtWidgets.QMainWindow, window_design.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        # Brain
        self.calculator = Calculator()

        # Widgets
        #  Buttons
        self.area_tab_pushButton_add_detail.clicked.connect(self.add_detail)
        self.area_tab_pushButton_delete_detail.clicked.connect(self.delete_detail)
        self.area_tab_pushButton_delete_all_details.clicked.connect(self.delete_all_details)
        self.area_tab_pushButton_get_result.clicked.connect(self.get_area)
        self.place_details_pushButton.clicked.connect(self.place_details)
        self.detail_tab_pushButton_delete_detail.clicked.connect(self.delete_detail_clean_area)
        self.detail_tab_pushButton_delete_all_details.clicked.connect(self.delete_all_details_clean_area)
        self.detail_tab_pushButton_add_area.clicked.connect(self.save_main_area)
        self.detail_tab_pushButton_add_detail.clicked.connect(self.add_detail_to_place)

        # Canvas
        self.scene = QtWidgets.QGraphicsScene()
        self.graphicsView.setScene(self.scene)

        # Main area where all details will be placed
        self.main_area = Detail(width=0, height=0)
        # All details that should be placed on main area
        self.details = []
        # Free spaces where details can be placed
        self.free_spaces = []

        # Origins
        #   Points where free areas is placed
        self.origins = [(0, 0), ]

    # TODO: Make base method to remove this delicious copy-paste
    @exc_safe
    def add_detail(self):
        try:
            width = int(self.area_tab_textEdit_detail_width.toPlainText())
        except Exception as e:
            log.error(f'Width is set not correctly. {e}')
            return

        try:
            height = int(self.area_tab_textEdit_detail_height.toPlainText())
        except Exception as e:
            log.error(f'Height is set not correctly. {e}')
            return

        detail = Detail(width=width, height=height)

        try:
            rowPosition = self.area_tab_tableWidget.rowCount()
            self.area_tab_tableWidget.insertRow(rowPosition)
            self.area_tab_tableWidget.setItem(rowPosition, 0, QtWidgets.QTableWidgetItem(str(detail.width)))
            self.area_tab_tableWidget.setItem(rowPosition, 1, QtWidgets.QTableWidgetItem(str(detail.height)))
        except Exception as e:
            log.error(f"Cannot insert row: {e}")

        self.area_tab_textEdit_detail_width.clear()
        self.area_tab_textEdit_detail_height.clear()

    @exc_safe
    def delete_detail(self):
        selected_rows = [x.row() for x in self.area_tab_tableWidget.selectedIndexes()]
        for selected_row in selected_rows:
            self.area_tab_tableWidget.removeRow(selected_row)

    @exc_safe
    def delete_all_details(self):
        self.area_tab_tableWidget.setRowCount(0)

    @exc_safe
    def get_area(self):
        summary_area = 0
        for index in range(self.area_tab_tableWidget.rowCount()):
            width, height = (int(self.area_tab_tableWidget.item(index, 0).text()),
                             int(self.area_tab_tableWidget.item(index, 1).text()))
            detail = Detail(width=width,
                            height=height)
            summary_area += detail.area()

        self.clean_area()
        text = self.scene.addText(f"{summary_area} мм²")
        text.setOpacity(0.5)
        text.setScale(5)

        self.summary_area_label.setText(f"{summary_area} мм²")

    @exc_safe
    def save_main_area(self):
        data = json.dumps({'width': self.detail_tab_textEdit_area_width.toPlainText(),
                           'height': self.detail_tab_textEdit_area_height.toPlainText()})
        try:
            validated = DetailSchema().loads(data)
        except:
            error_dialog = QtWidgets.QErrorMessage()
            error_dialog.showMessage('Укажите параметры доступной для размещения деталей площади.')
            error_dialog.setWindowTitle("Предпупреждение")
            return

        main_area = Detail(validated['width'], validated['height'])

        self.main_area = main_area
        self.detail_tab_pushButton_add_area.setText("Изменить")

    @exc_safe
    def place_details(self):
        self.clean_area()
        self.draw_main_area()
        self.draw_all_details()

    @exc_safe
    def draw_main_area(self):
        pen = QtGui.QPen(QtCore.Qt.black)
        pen.setStyle(QtCore.Qt.DashLine)

        area_rectangle = QtCore.QRectF(QtCore.QPointF(0, 0), QtCore.QSizeF(self.main_area.width, self.main_area.height))
        self.scene.addRect(area_rectangle, pen)

        # text = self.scene.addText("Общая площадь")
        # text.setOpacity(0.5)
        # text.setPos(self.main_area.center_point_width - 10, self.main_area.center_point_height - 10)

    def draw_all_details(self):
        details = []
        drawings = []

        # Clean previous measurements
        self.free_spaces = []
        self.free_spaces.append(self.main_area)

        for index in range(self.detail_tab_tableWidget.rowCount()):
            width, height = (int(self.detail_tab_tableWidget.item(index, 0).text()),
                             int(self.detail_tab_tableWidget.item(index, 1).text()))
            detail = Detail(width=width,
                            height=height)
            details.append(detail)

        summary_detail_area = sum([detail.area() for detail in details])
        if summary_detail_area > self.main_area.area():
            error_dialog = QtWidgets.QErrorMessage()
            error_dialog.showMessage('Внимани! Все детали не поместятся на указанную площадь.')

        sorted_details = sorted(details, key=operator.attrgetter('area_for_sorting'), reverse=True)

        # for detail in details:
        for detail in sorted_details:
            for free_space in self.free_spaces:
                result = self.calculator.find_place(free_space, detail)
                if not result:
                    index += 1
                    continue
                detail, cut_area_1, cut_area_2 = result
                drawings.append(detail)
                # Delete already used free space
                self.free_spaces.remove(free_space)
                # Add freshly created free spaces
                for cut in [cut_area_1, cut_area_2]:
                    if cut.fit_in_main_area(self.main_area):
                        self.free_spaces.append(cut)
                break

            self.free_spaces = self.calculator.update_free_spaces(self.free_spaces)
            log.info(f"Number of free spaces: {len(self.free_spaces)}")
            log.info(f"Free spaces: {[free_space.coordinates for free_space in self.free_spaces]}")

        for drawing in drawings:
            x0, y0, x1, y1 = drawing.coordinates[0][0],\
                             drawing.coordinates[0][1],\
                             drawing.coordinates[1][0],\
                             drawing.coordinates[1][1]
            detail_rectangle = QtCore.QRectF(QtCore.QPointF(x0, y0),
                                             QtCore.QSizeF(drawing.width, drawing.height))
            self.scene.addRect(detail_rectangle, detail_pen, detail_color)

            text = self.scene.addText(f"{drawing.width}x{drawing.height}")
            text.setOpacity(0.8)
            text.setPos(drawing.center_point_width - int(len(f"{drawing.width}x{drawing.height}")*3.5), drawing.center_point_height - 10)

        for num, space in enumerate(self.free_spaces):
            x0, y0, x1, y1 = space.coordinates[0][0],\
                             space.coordinates[0][1],\
                             space.coordinates[1][0],\
                             space.coordinates[1][1]
            space_rectangle = QtCore.QRectF(QtCore.QPointF(x0, y0),
                                            QtCore.QSizeF(space.width, space.height))
            self.scene.addRect(space_rectangle, free_space_pen, free_space_color)

            text = self.scene.addText(f"{num+1}")
            text.setOpacity(0.5)
            text.setPos(space.center_point_width - 10, space.center_point_height - 10)

    @exc_safe
    def add_detail_to_place(self):
        dimensions = json.dumps({"width": self.detail_tab_textEdit_detail_width.toPlainText(),
                                 "height": self.detail_tab_textEdit_detail_height.toPlainText()})
        try:
            data = MainAreaSchema().loads(dimensions)
        except:
            error_dialog = QtWidgets.QErrorMessage()
            error_dialog.showMessage('Укажите параметры детали.')
            error_dialog.setWindowTitle("Предпупреждение")
            return

        detail = Detail(width=data['width'], height=data['height'])

        try:
            rowPosition = self.detail_tab_tableWidget.rowCount()
            self.detail_tab_tableWidget.insertRow(rowPosition)
            self.detail_tab_tableWidget.setItem(rowPosition, 0, QtWidgets.QTableWidgetItem(str(detail.width)))
            self.detail_tab_tableWidget.setItem(rowPosition, 1, QtWidgets.QTableWidgetItem(str(detail.height)))
        except Exception as e:
            log.error(f"Cannot insert row: {e}")
            return

        self.detail_tab_textEdit_detail_width.clear()
        self.detail_tab_textEdit_detail_height.clear()

    @exc_safe
    def clean_area(self):
        self.scene = QtWidgets.QGraphicsScene()
        self.graphicsView.setScene(self.scene)

    @exc_safe
    def delete_detail_clean_area(self):
        self.clean_area()
        selected_rows = [x.row() for x in self.detail_tab_tableWidget.selectedIndexes()]
        for selected_row in selected_rows:
            self.detail_tab_tableWidget.removeRow(selected_row)

    @exc_safe
    def delete_all_details_clean_area(self):
        self.detail_tab_tableWidget.setRowCount(0)
