from source.logger import log
import traceback


def exc_safe(func):
    def wrapper(self=None):
        try:
            return func(self)
        except Exception as e:
            log.error(f'Error occurred in function: `{func.__name__}`: {e.__repr__} \n\t[traceback] :-: {traceback.format_exc()}')
    return wrapper
