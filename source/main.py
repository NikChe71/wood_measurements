import sys

from PyQt5 import QtWidgets
from source.application import App
from source.logger import log


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = App()
    log.info(f'Application window created.')
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
