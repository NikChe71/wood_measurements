import logging

logging.basicConfig(filename='wood_measurements.log', level=logging.DEBUG)
log = logging.getLogger('wood_utils')
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s - %(pathname)s:%(lineno)d')
ch.setFormatter(formatter)

log.addHandler(ch)
