from marshmallow import Schema, fields


class DetailSchema(Schema):
    width = fields.Int()
    height = fields.Int()


class MainAreaSchema(Schema):
    width = fields.Int()
    height = fields.Int()
